#!/bin/bash
# Option to install on non admin right-server. $HOME/bin must be in path
echo "Where do you want to install 'server'?
Make sure it's in your PATH
1) ${HOME}/bin
2) /usr/local/bin
"
read ans
if [ $ans == "1" ]
then
  echo "<#- Creating soft-link to ${HOME}/bin called server #>"
  DIR=$(pwd)
  mkdir -p ${HOME}/bin
  ln -s ${DIR}/main.py ${HOME}/bin/server > /dev/null 2>&1
  ls ${HOME}/bin/server > /dev/null 2>&1
  if [ $? == 0 ]
  then
    printf "<#- Sucessful install #>\n"
  else
    printf "<#- Unsucessful install #>\n"
  fi

elif [ $ans -eq "2" ]
then
  echo "<#- Creating soft-link to ${HOME}/bin called server #>"
  DIR=$(pwd)
  mkdir -p ${HOME}/bin
  ln -s ${DIR}/main.py ${HOME}/bin/server > /dev/null 2>&1
  sudo ln -s ${DIR}/main.py /usr/local/bin/server
  ls /usr/local/bin/server > /dev/null 2>&1
  if [ $? == 0 ]
  then
    printf "<#- Sucessful install #>\n"
  else
    printf "<#- Unsucessful install #>\n"
  fi
else
  echo "$ans not an option"
fi


echo "<#- Usage: server -h #>"
