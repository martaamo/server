#!/bin/bash
#LIST=$(openstack server list)
# Print name
#echo $LIST | awk '{print $5}'
#
# This will overwrite basic .servers.ini file,
# rename it and use -c option to select it, if you want to keep several config files
#
file=${HOME}/.servers.ini
key="${HOME}/.ssh/<YOUR_KEY_HERE>"
user="ubuntu"
openstack server list > list.txt
# Cleanup last config
rm $file
#cat list.txt | awk '{print $4}' | grep -v Name
for line in $(cat list.txt | awk '{print $4}' | grep -v Name)
do
echo "[${line}]" >> $file
echo "key = $key" >> $file
ip=$(cat list.txt | grep $line | awk '{print $9}')
echo "port = 22" >> $file
echo "user = $user " >> $file
echo "ip = $ip" >> $file
done

# Cleanup
rm list.txt
