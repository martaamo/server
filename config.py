#!/usr/bin/python
import os
import configparser

HOME = os.environ['HOME']
KEY = ""
backup_ip = ""
user = ""

def createConfig():
    config = configparser.ConfigParser()
    name = input("What is the server name")
    key = input("Which key will be used (Full path): ")
    ip = input("What ip address will get the backup: ")
    user = input("What user will be used(remote): ")
    config[name] = {'key':  key,
                    'ip': ip,
                    'user': user}
    with open(HOME + '/.servers.ini', 'w') as configfile:
        config.write(configfile)

def addServer(name):
    config = configparser.ConfigParser()
    KEY = input("Which key will be used (Full path): ")
    IP = input("What ip address will get the backup: ")
    USER = input("What user will be used(remote): ")
    PORT = input("Port number (standard ssh= 22): ")
    config[name] = {'key':  KEY,
                        'ip': IP,
                        'port': PORT,
                        'user': USER}
    with open(HOME + '/.servers.ini', 'a') as configfile:
        config.write(configfile)




def main():
    createConfig()
    print("\n")

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print("\nInterrupted\n")


