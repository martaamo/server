#!/usr/bin/python3
import os
import argparse
import config
import configparser

# Globals
HOME = os.environ['HOME']
CONFIG = HOME + "/.servers.ini"
USER = ""

###########################################
# Options parser
parser = argparse.ArgumentParser(prog='server')
parser.add_argument('-v', '--verbose', dest="verbose", help='Turn verbosity on', default=False, action="store_true")
parser.add_argument('-c', '--config', dest="config", metavar= '<Config file to use>', default=CONFIG)
parser.add_argument('-p', '--print', dest="print", help='Print Config file', default=False, action="store_true")
parser.add_argument('--proxy', dest="proxy", help='Proxy', default=False, action="store_true")
parser.add_argument('-r', '--reconfigure ', dest="reconfigure", help="Reconfigure config", default=False,
                    action="store_true")
parser.add_argument('-a', '--add', dest="addserver", metavar="<Add server>")
parser.add_argument('-u', '--user ', dest="user", metavar="<User name>")
parser.add_argument('-s', '--server', dest="server", metavar="<Server name>")
arguments = parser.parse_args()

VERBOSE = arguments.verbose
RECONFIGURE = arguments.reconfigure
CONFIG = str(arguments.config)
ADDSERVER = arguments.addserver
SERVER = arguments.server
PRINT = arguments.print
PROXY = arguments.proxy
USER = arguments.user


##########################################

def reconfigure():
    if RECONFIGURE:
        config.main()


def addServer():
    if ADDSERVER:
        config.addServer(ADDSERVER)


####################################
# Config file
if not os.path.isfile(HOME + "/.servers.ini"):
    config.main()

reconfigure()

#####################################


def verbose(text):
    if VERBOSE:
        print(text)


def connect():
    conf = configparser.ConfigParser()
    conf.read(HOME + '/.servers.ini')
    ip = conf[SERVER]['ip']
    if USER:
        user = USER
    else:
        user = conf[SERVER]['user']
    key = conf[SERVER]['key']
    port = conf[SERVER]['port']
    if PROXY:
        # Proxy
        os.system("ssh -i" + key + " -D " + port + " " + user + "@" + ip)
    elif port != 22:
        os.system("ssh -i" + key + " -p " + port + " " + user + "@" + ip)
        #print("ssh -i " + key + " -p " + port + " " + user + "@" + ip)
    else:
        os.system("ssh -i " + key + " " + user + "@" + ip)

def printConfig():
    if PRINT:
        f = open(HOME + '/.servers.ini', 'r')
        configFile = f.read()
        print(configFile)
        f.close()





def main():
    printConfig()
    addServer()
    if SERVER:
        connect()






if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print("\nInterrupted\n")
