# Server
Program to save ssh details and connect to servers.

# Install

Run install.sh script. Choose where to install, depending on if you have admin rights or not. Make sure install location is in PATH.

# Update
Install is just a symlink, git pull repo and its updated.

# openstackServers.sh
This script will add all servers form "openstack server list" to the config file.

EDIT IN FILE IS NEEDED FOR SCRIPT TO WORK:
* Find line with <YOUR_KEY_HERE> and put your private key there.

THIS WILL OVERWRITE ".servers.ini", if you want a seperate openstack config file you can do so by editing the file

* Change file variable.
* Specify the new file with -c option when running the program

# Usage

use -h for help, all options are there.
```bash
server -h
```

## Example

```bash
server -a testServer # Configure test server and add to config file
server -s testServer # connect to testServer
server -c special.ini -s manager # Connect to manager in special.ini file
server -p # Print config
```

# Inifile layout
```ini
[EXAMPLE] # <- Server name
key = <FULL PATH TO SSH KEY>
port = 22
user = <USER>
ip = <IP>
```

